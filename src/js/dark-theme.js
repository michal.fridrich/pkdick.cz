const params = new Proxy(new URLSearchParams(window.location.search), {
  get: (searchParams, prop) => searchParams.get(prop),
});
let darkmode = params.dark_mode;
if (darkmode == "1") {
    jQuery('body').addClass('dark-mode');
    localStorage.setItem('color-mode', 'dark');
    jQuery('#color-mode-toggle').addClass('dark-mode');
}

jQuery('#color-mode-toggle').on('click', function() {
  if (jQuery('body').hasClass('dark-mode')) {
    jQuery('body').removeClass('dark-mode');
    localStorage.removeItem('color-mode');
    jQuery('#color-mode-toggle').removeClass('dark-mode');
  } else {
    jQuery('body').addClass('dark-mode');
    localStorage.setItem('color-mode', 'dark');
    jQuery('#color-mode-toggle').addClass('dark-mode');
  }
});