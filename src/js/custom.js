( function () {
    'use strict';

    // Focus input if Searchform is empty
    [].forEach.call( document.querySelectorAll( '.search-form' ), ( el ) => {
        el.addEventListener( 'submit', function ( e ) {
            var search = el.querySelector( 'input' );
            if ( search.value.length < 1 ) {
                e.preventDefault();
                search.focus();
            }
        } );
    } );

    // Initialize Popovers: https://getbootstrap.com/docs/5.0/components/popovers
    var popoverTriggerList = [].slice.call( document.querySelectorAll( '[data-bs-toggle="popover"]' ) );
    var popoverList = popoverTriggerList.map( function ( popoverTriggerEl ) {
        return new bootstrap.Popover( popoverTriggerEl, {
            trigger: 'focus',
        } );
    } );
} )();

/* Hero - Homepage */
function setSidebarHeight() {
    let windowHeight = window.innerHeight;
    var menuUtilsHeight = jQuery('#menu-utils').outerHeight();
    let windowHeightWithoutMenu = windowHeight - menuUtilsHeight;
    jQuery('#hero').css({"height": windowHeightWithoutMenu + "px"});
    jQuery('#header').css({"height": windowHeightWithoutMenu + "px"});
}
window.addEventListener('resize', setSidebarHeight);
setSidebarHeight();

// Sidebar toggle behavior
jQuery(function() {
    jQuery('#header-navbar--collapse, #header-navbar--logo').on('click', function() {
        jQuery('#header-navbar--vertical, #main, #header-navbar--collapse, #header-navbar--logo, #footer, #footer-2').toggleClass('nonactive');
        console.log("Test");
        localStorage.setItem("menu-state", "closed");
        jQuery( "#mydiv" ).hasClass( "foo" )
    });
    jQuery('#header-navbar--tags').on('click', function() {
        jQuery('#header-navbar--tags').toggleClass('active');
    });
    jQuery('#header-navbar--tags, #header-navbar--search').on('click', function() {
        document.getElementById("content").scrollIntoView();
    });
    jQuery('.post-list-preview-btn').on('click', function() {
        jQuery(this).toggleClass('active');
    });
});