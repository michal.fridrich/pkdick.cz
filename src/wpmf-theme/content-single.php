<?php
/**
 * The template for displaying content in the single.php template.
 *
 */

$orig_nazev = get_post_meta(get_the_ID(), 'cpf_originalni_nazev', true);
$vydavatelstvi = get_post_meta(get_the_ID(), 'cpf_vydavatelstvi', true);
$rok_vydani = get_post_meta(get_the_ID(), 'cpf_rok_vydani', true);
$category_list = get_the_category_list( __( ', ', 'wpmf-theme-v1' ) );
$terms_orig_nazev = get_the_terms(get_the_ID(), 'originalni_nazev');
$terms_vydavatelstvi = get_the_terms(get_the_ID(), 'vydavatelstvi');
$terms_rok = get_the_terms(get_the_ID(), 'rok_vydani');

?>
<article id="post-<?php the_ID(); ?>" <?php post_class("row post-body-content"); ?>>

	<?php
	if ( has_post_thumbnail() ) : ?>
	<div class="col-auto">
		<figure class="figure d-block">
			<?php echo '<div class="">' . get_the_post_thumbnail( get_the_ID(), 'large' ) . '</div>'; ?>
		</figure>
	</div>
	<?php endif; ?>
	<div class="col">
		<div class="post-body position-relative">

			<?php if(is_category() || is_archive() || is_tag() || is_search()) : ?>
			<h3 class="title-post c-theme-primary"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'wpmf-theme-v1' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
			<hr>
			<? endif; ?>

			<?php if ( $terms_orig_nazev && ! is_wp_error( $terms_orig_nazev ) ) : ?>
			<div class="mb-1">
				<span><?php esc_html_e( 'Originální název:', 'wpmf-theme-v1' ); ?></span>
				<span class="fw-bold">
				<?php
				echo '<ul class="px-0 custom-taxonomy-list d-inline-block mb-0">';
				foreach ($terms_orig_nazev as $term) {
				    echo '<li class="d-inline-block"><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
				};
				echo '</ul>';
				?>
				</span>
			</div>
			<?php endif; ?>

			<?php if ( $terms_vydavatelstvi && ! is_wp_error( $terms_vydavatelstvi ) ) : ?>
			<div class="mb-1">
				<span><?php esc_html_e( 'Vydavatelství:', 'wpmf-theme-v1' ); ?></span>
				<span class="fw-bold">
				<?php
				echo '<ul class="px-0 custom-taxonomy-list d-inline-block mb-0">';
				foreach ($terms_vydavatelstvi as $term) {
				    echo '<li class="d-inline-block"><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
				};
				echo '</ul>'; ?>
				</span>
			</div>
			<?php endif; ?>

			<?php if ( $terms_rok && ! is_wp_error( $terms_rok ) ) : ?>
			<div class="pb-3">
				<span><?php esc_html_e( 'Rok vydání:', 'wpmf-theme-v1' ); ?></span>
				<span class="fw-bold">
				<?php
				echo '<ul class="px-0 custom-taxonomy-list d-inline-block mb-0">';
				foreach ($terms_rok as $term) {
				    echo '<li class="d-inline-block"><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
				};
				echo '</ul>'; ?>

				</span>
			</div>
			<?php endif; ?>

			<?php if(is_category() || is_archive() || is_tag() || is_search()) : ?>
			<div class="post-list-preview collapse mt-3" id="post-collapse-<?php the_ID(); ?>">
			<?php endif; ?>

			<?php the_content(); ?>

			<?php if(is_category() || is_archive() || is_tag() || is_search()) : ?>
			</div>
			<?php endif; ?>

			<?php if(is_category() || is_archive() || is_tag() || is_search()) : ?>
			<div class="row">
				<div class="col-auto py-3">
					<button class="btn btn-outline rounded-pill post-list-preview-btn fs-smaller" type="button" data-bs-toggle="collapse" data-bs-target="#post-collapse-<?php the_ID(); ?>" aria-expanded="false" aria-controls="post-collapse-<?php the_ID(); ?>">
						Více
						<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-arrow-down-short" viewBox="0 0 16 16">
						  	<path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5A.5.5 0 0 1 8 4z"/>
						</svg>
						<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
  							<path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"/>
						</svg>
					 </button>
				</div>
			</div>
			<?php endif; ?>
		</div><!-- /.entry-content -->
		<hr>
	</div>
</article><!-- /#post-<?php the_ID(); ?> -->
