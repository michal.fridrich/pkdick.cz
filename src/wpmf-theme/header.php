<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/safari-pinned-tab.svg" color="#50357e">
	<meta name="msapplication-TileColor" content="#50357e">
	<meta name="theme-color" content="#50357e">

	<?php wp_head(); ?>
</head>

<?php
	$navbar_scheme   = get_theme_mod( 'navbar_scheme', 'navbar-light bg-light' ); // Get custom meta-value.
	$navbar_position = get_theme_mod( 'navbar_position', 'static' ); // Get custom meta-value.
	$search_enabled  = get_theme_mod( 'search_enabled', '1' ); // Get custom meta-value.

?>

<body <?php body_class(''); ?> data-barba="wrapper">
<script type="text/javascript">
	if (localStorage.getItem("color-mode") === "dark") {
	  document.body.className += ' dark-mode';
	  };
</script>

<?php wp_body_open(); ?>

<a href="#main" class="visually-hidden-focusable"><?php esc_html_e( 'Skip to main content', 'wpmf-theme-v1' ); ?></a>

<div id="wrapper">
	<header class="d-none">
		<nav id="header" class="navbar navbar-expand-md <?php echo esc_attr( $navbar_scheme ); if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' fixed-top'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' fixed-bottom'; endif; if ( is_home() || is_front_page() ) : echo ' home'; endif; ?>">
			<div class="container">
				<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php
						$header_logo = get_theme_mod( 'header_logo' ); // Get custom meta-value.

						if ( ! empty( $header_logo ) ) :
					?>
						<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					<?php
						else :
							echo esc_attr( get_bloginfo( 'name', 'display' ) );
						endif;
					?>
				</a>

				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'wpmf-theme-v1' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div id="navbar" class="collapse navbar-collapse">
					<?php
						// Loading WordPress Custom Menu (theme_location).
						wp_nav_menu(
							array(
								'theme_location' => 'main-menu',
								'container'      => '',
								'menu_class'     => 'navbar-nav me-auto',
								'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
								'walker'         => new WP_Bootstrap_Navwalker(),
							)
						);

						if ( '1' === $search_enabled ) :
					?>
							<form class="search-form my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<div class="input-group">
									<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Search', 'wpmf-theme-v1' ); ?>" title="<?php esc_attr_e( 'Search', 'wpmf-theme-v1' ); ?>" />
									<button type="submit" name="submit" class="btn btn-outline-secondary"><?php esc_html_e( 'Search', 'wpmf-theme-v1' ); ?></button>
								</div>
							</form>
					<?php
						endif;
					?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /#header -->
	</header>

    <!-- Vertical navbar -->
    <header id="header" class="header">
        <div id="header-navbar--vertical" class="header-navbar--vertical">
        	<div class="header-navbar--vertical-content px-3 py-4">
        		<!-- Logo -->
        		<div class="logo mb-3">
					<a class="font-heading fs-biggest fw-700" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<?php
							$header_logo = get_theme_mod( 'header_logo' ); // Get custom meta-value.

							if ( ! empty( $header_logo ) ) :
						?>
							<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
						<?php
							else :
								echo esc_attr( get_bloginfo( 'name', 'display' ) );
							endif;
						?>
					</a>
				</div>
				<!-- Menu -->
		        <?php
		            // Loading WordPress Custom Menu (theme_location).
		            wp_nav_menu(
		                array(
		                    'theme_location' => 'main-menu',
		                    'container'      => 'ul',
		                    'menu_class'     => 'nav main-menu header-navbar-nav',
		                    'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
		                    'walker'         => new WP_Bootstrap_Navwalker()
		                )
		            );
		        ?>
        	</div>
            <!-- Credits -->
            <div class="fs-small px-3 pb-2">
            	<p>&copy; X2001-<?php echo date("Y"); ?> Radomil Dojiva. Některá práva vyhrazena.</p>
            	<p>Použijete-li zde uvedené informace ke komerčním účelům, nezapomeňte uvést tento zdroj. Za správnost obsahu neručím, nejsem robot.</p>
            	<p>Připomínky k obsahu těchto stránek zasílejte na rdojiva(zavináč)2d.cz. Design &amp; kód <a href="https://www.3drich.cz" title="Michal Fridrich - Portfolio">Michal Fridrich</a></p>
        		<button type="button" class="btn btn-outline rounded-pill color-mode-toggle mb-3" title="Přepněte barevný režim" alt="Tmavý režim" id="color-mode-toggle">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-moon-stars" viewBox="0 0 16 16">
					  	<path d="M6 .278a.768.768 0 0 1 .08.858 7.208 7.208 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277.527 0 1.04-.055 1.533-.16a.787.787 0 0 1 .81.316.733.733 0 0 1-.031.893A8.349 8.349 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.752.752 0 0 1 6 .278zM4.858 1.311A7.269 7.269 0 0 0 1.025 7.71c0 4.02 3.279 7.276 7.319 7.276a7.316 7.316 0 0 0 5.205-2.162c-.337.042-.68.063-1.029.063-4.61 0-8.343-3.714-8.343-8.29 0-1.167.242-2.278.681-3.286z"/>
					  	<path d="M10.794 3.148a.217.217 0 0 1 .412 0l.387 1.162c.173.518.579.924 1.097 1.097l1.162.387a.217.217 0 0 1 0 .412l-1.162.387a1.734 1.734 0 0 0-1.097 1.097l-.387 1.162a.217.217 0 0 1-.412 0l-.387-1.162A1.734 1.734 0 0 0 9.31 6.593l-1.162-.387a.217.217 0 0 1 0-.412l1.162-.387a1.734 1.734 0 0 0 1.097-1.097l.387-1.162zM13.863.099a.145.145 0 0 1 .274 0l.258.774c.115.346.386.617.732.732l.774.258a.145.145 0 0 1 0 .274l-.774.258a1.156 1.156 0 0 0-.732.732l-.258.774a.145.145 0 0 1-.274 0l-.258-.774a1.156 1.156 0 0 0-.732-.732l-.774-.258a.145.145 0 0 1 0-.274l.774-.258c.346-.115.617-.386.732-.732L13.863.1z"/>
					</svg>
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-brightness-high" viewBox="0 0 16 16">
					  	<path d="M8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z"/>
					</svg>
        		</button>
			</div>
            <!-- Menu Toggle buttons --collapse, -- -->
            <button id="header-navbar--collapse" type="button" class="btn btn-outline rounded-pill header-navbar--collapse d-md-none header-navbar--collapse-floating position-fixed top-0 end-0 me-4 mt-4">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-text-center d-none" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M4 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-chevron-bar-right" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M4.146 3.646a.5.5 0 0 0 0 .708L7.793 8l-3.647 3.646a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708 0zM11.5 1a.5.5 0 0 1 .5.5v13a.5.5 0 0 1-1 0v-13a.5.5 0 0 1 .5-.5z"/>
                </svg>
            </button>

        </div>
    </header>
    <!-- End vertical navbar -->

	<main id="main" class="page-content" data-barba="container" data-barba-namespace="home">
		<div class="container-fluid px-0">
			<div class="row mx-0">
				<div id="hero" class="col-12 col-lg-5 position-relative px-0 h-lg-100v">
					<div class="hero-content position-fixed m-0 top-0 start-0">
						<img class="hero-img hero-img-0 hero-img-dark" src="/wp-content/uploads/2023/02/pkd-01.png">
						<img class="hero-img hero-img-0 hero-img-light" src="/wp-content/uploads/2023/04/pkd-01-light.png">
					</div>
					<div class="hero-content position-fixed m-0 top-0 start-0">
						<img class="hero-img hero-img-1 hero-img-dark" src="/wp-content/uploads/2023/02/pkd-02.png">
						<img class="hero-img hero-img-1 hero-img-light" src="/wp-content/uploads/2023/04/pkd-02-light.png">
					</div>
					<h2 class="position-relative position-lg-fixed col-lg-3 bottom-lg-0 start-lg-0 p-4"><?php echo esc_attr( get_bloginfo( 'description', 'display' ) ); ?></h2>
				</div>
				<div id="content" class="col-12 col-lg-7 page-body position-relative px-3 px-md-5 pb-5">
					<!-- Operation menu top -->
					<div id="menu-utils" class="sticky-top">
						<div class="row py-4">
							<form class="search-form col" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<div class="input-group">
									<input id="header-navbar--search" type="text" name="s" class="form-control form-control-input header-navbar--search" placeholder="<?php esc_attr_e( 'Zadejte co hledáte', 'wpmf-theme-v1' ); ?>" title="<?php esc_attr_e( 'Zde zadejte co hledáte', 'wpmf-theme-v1' ); ?>" />
									<button type="submit" name="submit" class="btn btn-outline form-control-submit">
										<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-search me-1" viewBox="0 0 16 16">
										  	<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
										</svg>
										<span class="d-none"><?php esc_html_e( 'Search', 'wpmf-theme-v1' ); ?></span>
									</button>
								</div>
							</form>
				            <a id="header-navbar--tags" href="#menu-utils" type="button" class="col-auto ms-auto me-2 btn btn-outline rounded-pill header-navbar--tags collapsed" data-bs-toggle="collapse" data-bs-target="#tags-container" aria-expanded="false" aria-controls="tags-container">
				                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-tags-fill tags-collapsed" viewBox="0 0 16 16">
								  	<path d="M2 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 2 6.586V2zm3.5 4a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
								  	<path d="M1.293 7.793A1 1 0 0 1 1 7.086V2a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l.043-.043-7.457-7.457z"/>
								</svg>
								<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg tags-close" viewBox="0 0 16 16">
  									<path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
								</svg>
				            </a>

				            <button id="header-navbar--collapse" type="button" class="col-auto ms-auto btn btn-outline rounded-pill header-navbar--collapse d-md-none me-2 me-md-0">
				                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-text-left" viewBox="0 0 16 16">
								  	<path fill-rule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
								</svg>
				                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-chevron-bar-right" viewBox="0 0 16 16">
				                    <path fill-rule="evenodd" d="M4.146 3.646a.5.5 0 0 0 0 .708L7.793 8l-3.647 3.646a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708 0zM11.5 1a.5.5 0 0 1 .5.5v13a.5.5 0 0 1-1 0v-13a.5.5 0 0 1 .5-.5z"/>
				                </svg>
				            </button>
			        	</div>
					</div>
					<div id="tags-container" class="tags-container collapse px-3 px-md-0">
						<h4 class="mt-4">Rok vydání</h4>
						<ul id="tags-list" class="px-0 py-3">
						<?php
						$terms = get_terms(
						    array(
						        'taxonomy'   => 'rok_vydani',
						        'hide_empty' => false,
						    )
						);
						if ( ! empty( $terms ) && is_array( $terms ) ) {
						    foreach ( $terms as $term ) { ?>
						        <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="btn btn-outline rounded-pill mb-3 me-2">
						            <?php echo $term->name; ?>
						        </a><?php
						    }
						} ?>
						</ul>
						<h4>Vydavatel/autor</h4>
						<ul id="tags-list" class="px-0 py-3">
						<?php
						$terms = get_terms(
						    array(
						        'taxonomy'   => 'vydavatelstvi',
						        'hide_empty' => false,
						    )
						);
						if ( ! empty( $terms ) && is_array( $terms ) ) {
						    foreach ( $terms as $term ) { ?>
						        <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="btn btn-outline rounded-pill mb-3 me-2">
						            <?php echo $term->name; ?>
						        </a><?php
						    }
						} ?>
						</ul>
						<h4>Originální název</h4>
						<ul id="tags-list" class="px-0 py-3">
						<?php
						$terms = get_terms(
						    array(
						        'taxonomy'   => 'originalni_nazev',
						        'hide_empty' => false,
						    )
						);
						if ( ! empty( $terms ) && is_array( $terms ) ) {
						    foreach ( $terms as $term ) { ?>
						        <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="btn btn-outline rounded-pill mb-3 me-2">
						            <?php echo $term->name; ?>
						        </a><?php
						    }
						} ?>
						</ul>
					</div>
					<!-- /Operation menu top -->
					<div id="post-<?php the_ID(); ?>" <?php post_class( 'content mt-3' ); ?>>
						<header class="page-header pb-3 font-heading">
							<?php
							$category_list = get_the_category_list( __( ', ', 'wpmf-theme-v1' ) );
							if(is_single()) : ?>
							<p>
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-bookmark-fill" viewBox="0 0 16 16">
  									<path d="M2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2z"/>
								</svg>
								<?php echo $category_list; ?>
							</p>
							<hr>
							<?php endif; ?>

							<?php if (is_search()) : ?>
							<h1 class="page-title font-heading"><?php printf( esc_html__( 'Search Results for: %s', 'wpmf-theme-v1' ), get_search_query() ); ?></h1>
							<hr>
							<?php endif; ?>
							<!-- Title post/page -->
							<?php if (is_single() || is_page()) : ?>
							<?php $category_list = get_the_category_list( __( ', ', 'wpmf-theme-v1' ) ); ?>
							<h1 class="title-post font-heading"><?php the_title(); ?></h1>
							<hr>
							<?php endif; ?>
							<?php if(is_category() && is_archive()) : ?>
							<div class="row">
								<h1 class="title-post col">
									<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-bookmark-fill me-3" viewBox="0 0 16 16">
	  									<path d="M2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2z"/>
									</svg><?php printf( single_cat_title( '', false ) ); ?>
								</h1>
								<div class="col-auto">
								<?php wpmf_theme_v1_content_nav( 'nav-above' ); ?>
								</div>
							</div>
							<hr class="mb-3">
							<? endif; ?>
							<?php if(is_tax()) : ?>
							<div class="row">
								<h1 class="title-post col">
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tag-fill me-3" viewBox="0 0 16 16">
									  	<path d="M2 1a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l4.586-4.586a1 1 0 0 0 0-1.414l-7-7A1 1 0 0 0 6.586 1H2zm4 3.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
									</svg><?php single_term_title(); ?>
								</h1>
								<div class="col-auto">
								<?php wpmf_theme_v1_content_nav( 'nav-above' ); ?>
								</div>
							</div>
							<hr class="mb-3">
							<? endif; ?>
							<?php if(is_tag()) : ?>
							<div class="row mb-3">
								<h1 class="title-post col">
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tag-fill me-3" viewBox="0 0 16 16">
									  	<path d="M2 1a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l4.586-4.586a1 1 0 0 0 0-1.414l-7-7A1 1 0 0 0 6.586 1H2zm4 3.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
									</svg><?php printf( single_cat_title( '', false ) ); ?>
								</h1>
								<div class="col-auto">
								<?php wpmf_theme_v1_content_nav( 'nav-above' ); ?>
								</div>
							</div>
							<hr class="mb-3">
							<? endif; ?>
						</header>


