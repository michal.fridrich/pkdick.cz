��    _                      	  
          &   4     [  (   q  %   �  -  �     �	  
   �	  ?   
     B
     V
  
   d
     o
  
   |
     �
     �
     �
     �
  5   �
                    #     (     .     7     G     T     `     s     z  1   �     �     �  a   �  	   C     M     b     g     n     |     �  
   �     �  	   �     �     �     �     �     �            $        <     E  	   U     _     l     {     �     �     �  M   �     �     �          ,     >  _   S     �     �     �  P   �  �     �   �  �   >     �     �     �  
          /        I  9   ]  )   �  $   �  R   �  ,   9     f      ~     �     �  -  �  
   �  
   �       ,        C  3   _  '   �  2  �     �     �  ;         <     N     [     g     z     �  
   �     �     �  ;   �          $  	   +     5     =     D     Q     `     r     �  	   �     �     �     �     �  k   �     g     t     �     �     �     �     �     �       
   '     2     A     W     `     u  	   �     �     �     �     �     �     �       
        $     B     ]  ^   i     �     �     �          ,  g   I  	   �     �     �  >   �  �     �   �  �   H     �     �  $   	   
   .      9   0   J      {   F   �   ,   �   '   !  (   *!  #   S!     w!     �!     �!     �!                     O       A       G      J      !       \   ^   R       >   0   #             2              1      T   B   W          Y   $   '                             D   K      E   =   ?   U       Q      _   3              ]   5      [       :   %         F         @   +   C   	   ,   ;   Z   X   7              <   "   6   8   )       V   9   (       &           *       L   P   -           H   /                                   4   I   N             M   .   
   S    %1$s ago %1$s, %2$s %s Comment %s Comments &copy; %1$s %2$s. All rights reserved. &larr; Older Comments <span class="%1$s">Posted in</span> %2$s <span class="%1$s">Tagged</span> %2$s <span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author-meta vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span> About %s Add a menu Apologies, but no results were found for the requested archive. Author Archives: %s Blog Archives Blog Index Cancel reply Categories Category Archives: %s Comment Comment navigation Comments are closed. Continue reading <span class="meta-nav">&rarr;</span> Daily Archives: %s Dark Default Edit Email Featured Fixed to bottom Fixed to top Footer menu Footer widget area Header Height: &gt;80px It looks like nothing was found at this location. Leave a Reply to %s Leave a comment Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Main menu Monthly Archives: %s Name Navbar Navbar Scheme Newer Comments &rarr; Newer posts Next image No Comments yet! Not found Nothing Found Older posts Page Page (Default) Page (Full width) Pages: Parent post Parent post linkPublished in %title Password Permalink to %s Pingback: Post Comment Previous image Primary Primary widget area Recent Posts Reply Save my name, email, and website in this browser for the next time I comment. Search Search Results for: %s Secondary widget area Show Searchfield? Skip to main content Sorry, but nothing matched your search criteria. Please try again with some different keywords. Static Submit Tag: %s This content is password protected. To view it please enter your password below. This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. This entry was posted in %1$s and tagged %2$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. This entry was posted in %1$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. Toggle navigation Upload Header Logo View all posts by %s WPMF Theme Website Wordpress, Bootsrap, Gulp etc. kickstart theme. Yearly Archives: %s You must be <a href="%s">logged in</a> to post a comment. Your Email address will not be published. Your comment is awaiting moderation. comments title%1$s Reply to &ldquo;%2$s&rdquo; %1$s Replies to &ldquo;%2$s&rdquo; comments titleOne Reply to &ldquo;%s&rdquo; https://them.es/starter monthly archives date formatF Y more yearly archives date formatY Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);
Project-Id-Version: WPMF Theme
POT-Creation-Date: 2022-11-27 14:47+0100
PO-Revision-Date: 2022-11-27 14:53+0100
Language-Team: Michal Fridrich <michal.fridrich@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: Michal Fridrich <michal.fridrich@gmail.com>
Language: cs_CZ
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Před %1$s %1$s, %2$s %s komentář   &copy; %1$s %2$s. Všechna práva vyhrazena. &larr; Starší komentáře <span class="%1$s">Přidáno do rubriky</span> %2$s <span class="%1$s">Štítky</span> %2$s <span class="sep">Přidáno </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="by-author"> <span class="sep"> autorem </span> <span class="author-meta vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span> O %s Přidat menu Omlouváme se, ale pro hledaný archiv nebylo nic nalezeno. Archiv autora: %s Archiv blogu Index blogu Zrušit komentář Rubriky Archivy rubrik: %s Komentář Procházení komentářů Komentáře jsou uzavřeny. Pokračovat ve čtení <span class="meta-nav">&rarr;</span> Denní archiv: %s Tmavá Výchozí Upravit E-mail Zvýrazněno Plovoucí dole Plovoucí nahoře Menu v patičce Widgety v patičce Hlavička Výška: &gt;80px Zdá se, že zde nic není. Zanechat komentář pro %s Přidat komentář Přihlášen jako <a href="%1$s">%2$s</a>. <a href="%3$s" title="Odhlásit se z toho účtu">Odhlásit?</a> Hlavní menu Měsíční archiv: %s Jméno Navigační lišta Schéma navigační lišty Novější komentáře &rarr; Novější příspěvky Další obrázek Prozatím žádné komentáře! Nenalezeno Nic nenalezeno Starší příspěvky Stránka Stránka (výchozí) Stránka (plná šířka) Stránky: Nadřazený příspěvek Published in %title Heslo Trvalý odkaz pro %s Zpětný odkaz: Přidat komentář Předchozí obrázek Primární Primární oblast pro widgety Nejnovější příspěvky Odpovědět Přeji si uložit jméno, e-mail a adresu webu v tomto prohlížeči pro budoucí komentáře. Vyhledat Výsledky hledání pro: %s Sekudární oblast pro widgety Zobrazovat vyhledávací panel? Přeskočit na hlavní obsah Omlouváme se, ale žádný obsah neodpovídá vašemu hledání. Zkuste zvolit jiná klíčová slova. Statická Odeslat Štítek: %s Tento obsah je chráněn heslem. Pro zobrazení zadejte heslo. Přidáno do rubriky <a href="%6$s">%5$s</a>. Uložte si <a href="%3$s" title="Trvalý odkaz pro %4$s" rel="bookmark">trvalý odkaz</a>. Přidáno do rubriky %1$s a označeno štítky %2$s autorem <a href="%6$s">%5$s</a>. Uložte si <a href="%3$s" title="Trvalý odkaz pro %4$s" rel="bookmark">trvalý odkaz</a>. Přidáno do rubriky %1$s autorem <a href="%6$s">%5$s</a>. Uložte si <a href="%3$s" title="Trvalý odkaz pro %4$s" rel="bookmark">trvalý odkaz</a>. Zapnout navigaci Nahrát logo do hlavičky Zobrazit všechny příspěvky od %s WPMF Theme Webová stránka Wordpress, Bootstrap, Gulp etc. kickstart theme. Roční archiv: %s Musíte být <a href="%s">přihlášen</a> pro přidání komentáře. Vaše e-mailová adresa nebude publikována. Váš komentář čeká na schválení. %1$s komentář pro &ldquo;%2$s&rdquo;   Jeden komentář k &ldquo;%s&rdquo; https://www.michalfridrich.com F Y více Y 