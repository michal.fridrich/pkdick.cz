<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side.
 *
 */

get_header();

the_post();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content post-body post-body-content' ); ?>>
		<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'wpmf-theme-v1' ),
					'after'  => '</div>',
				)
			);
			edit_post_link( esc_html__( 'Edit', 'wpmf-theme-v1' ), '<span class="edit-link">', '</span>' );
		?>
	</div><!-- /#post-<?php the_ID(); ?> -->
	<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	?>
</article><!-- /.col -->
<?php
get_footer();
