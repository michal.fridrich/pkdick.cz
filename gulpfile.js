// Gulp
const gulp = require('gulp');

// Env
const mode = require('gulp-mode')({
  modes: ["development", "stage", "production"],
  default: "development",
  verbose: false
});

// CSS
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const googlefonts = require('gulp-google-fonts');

// JS
const jsImport = require('gulp-js-import');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const stripDebug = require('gulp-strip-debug');

// Utils
const sourcemaps = require('gulp-sourcemaps');
const gutil = require('gulp-util');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const ftp = require('vinyl-ftp');
const ftpconfig = require('./ftpcred');
const rename = require('gulp-rename');
const newer = require('gulp-newer');
const strip = require('gulp-strip-comments');

// Live
const browserSync = require('browser-sync').create();

// Paths
const path = {
    src: './src/',
    dest: './src/wpmf-theme/',
    public: './app/public/wp-content/themes/wpmf-theme/',
    styles: {
        src: './src/scss/',
        dest: './src/wpmf-theme'
    },
    scripts: {
        src: './src/js/',
        dest: './src/wpmf-theme/js'
    },
    assets: {
        src: './src/assets/',
        dest: './src/wpmf-theme/assets'
    },
    images: {
        src: './src/assets/img/',
        dest: './src/wpmf-theme/assets/img'
    },
    remote: {
        host: ftpconfig.remote.host,
        dest: ftpconfig.remote.dest,
        user: ftpconfig.remote.user,
        pass: ftpconfig.remote.pass,
    }
};

gulp.task('dest-to-public', function() {
  // Add the newer pipe to pass through newer images only
  return gulp.src(path.dest + '/**/*')
      .pipe(newer(path.public))
      .pipe(strip.html())
      .pipe(gulp.dest(path.public));
});

gulp.task('theme-install', function() {
  return gulp.src(path.dest + '/**/*')
      .pipe(gulp.dest(path.public));
});

// Watchers
gulp.task('watch', function(){
    // Modes
    var isProduction = mode.production();
    var isStage = mode.stage();
    var isDevelopment = mode.development();
    if(isProduction) {
      console.log("PRODUCTION mode");
    };
    if(isStage) {
      console.log("STAGE mode");
    };
    if(isDevelopment) {
      console.log("DEV mode");
    };

    // Browser Sync Init
    /*browserSync.init({
        proxy: "http://pkdcz.local/",
    });*/

    // Watch Styles
    gulp.watch(path.styles.src + '**/*', gulp.series('css'));
    gulp.watch(path.styles.dest + '**/*.css', gulp.series('remote-deploy-css'));

    // Watch Scripts
    gulp.watch(path.scripts.src + '**/*', gulp.series('js'));
    gulp.watch(path.scripts.dest, gulp.series('remote-deploy-js'));

    //Watch Images
    gulp.watch(path.images.src + '**/*', gulp.series('imgmin'));
    gulp.watch(path.images.dest, gulp.series('remote-deploy-images'));

    //Watch Dest
    gulp.watch(path.dest + '**/*', gulp.series('dest-to-public'));
    gulp.watch(path.dest, gulp.series('remote-deploy-files'));

    // Browser Sync Reload
    //gulp.watch([path.dest + '**/*.php', path.dest + '**/*.css', path.dest + '**/*.js', path.dest + 'img/**/*']).on('change', browserSync.reload);
});

// Compile CSS
gulp.task('css', function(){
    const atimport = require("postcss-import");
    const postcss = require("gulp-postcss");
    //const purgecss = require("gulp-purgecss");

    return gulp
        //.src(mainCSS)
        .src(path.styles.src + '*.scss')
        .pipe( mode.development (
            sourcemaps.init()
            )
        )
        .pipe(sass().on('error', sass.logError))
        .pipe(
            mode.development (
                postcss([
                    atimport(),
                    autoprefixer()
                ])
            )
        )
        .pipe(
            mode.stage(
                postcss([
                    atimport(),
                    autoprefixer()
                ])
            )
        )
        .pipe(
            mode.production(
                postcss([
                    atimport(),
                    autoprefixer(),
                    cssnano()
                ])
            )
        )
        .pipe( mode.development(
            sourcemaps.write()
        ))
        .pipe(gulp.dest(path.styles.dest))
});

// Compile JS
gulp.task('js', function(){
    return gulp.src(path.scripts.src + 'script-logic.js')
        //.pipe(jsImport({hideConsole: true}))
        .pipe(jsImport({hideConsole: false}))
        .pipe(concat('scripts.bundle.js'))
        .pipe(mode.production(uglify()))
        //.pipe(stripDebug())
        .pipe(gulp.dest(path.scripts.dest));
});

// Minify Images
gulp.task("imgmin", imgMinify);

// Google Fonts
gulp.task('getfonts', function () {
  return gulp.src(path.src + 'gfonts.neon')
    .pipe(googlefonts())
    .pipe(gulp.dest(path.styles.src + 'fonts'));
});

// Icons: Copy bootstrap-icons from node_modules into /fonts
gulp.task('geticons', function() {
    var cloneSrc = gulp.src('./node_modules/bootstrap-icons/font/fonts/**/*')
        .pipe(gulp.dest(path.styles.src + '/fonts/bootstrap-icons'))
        .pipe(gulp.dest(path.assets.dest + '/fonts/bootstrap-icons'));
    var processSrc = gulp.src('./node_modules/bootstrap-icons/font/bootstrap-icons.scss')
        .pipe(rename("_bootstrap-icons.scss"))
        .pipe(gulp.dest(path.styles.src + 'vendors'));
    return merge(cloneSrc, processSrc);
});

// Copy resources
gulp.task('copyresources', function() {
    return merge([
        gulp.src('node_modules/bootstrap-icons/font/**/*.*')
            .pipe(gulp.dest(path.styles.dest + '/bootstrap-icons'))
      //gulp.src('./src/data/*.json').pipe(gulp.dest('./deploy/data'))
  ]);
});

// Imgmin
function imgMinify() {
    return gulp.src(path.images.src + "*.*")
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(path.images.dest));
};

// Deploy to remote server

var localCss = [
    path.dest + 'css**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];

var localJs = [
    path.dest + 'js**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];

var localImages = [
    path.dest + 'assets/img**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];

var localFiles = [
    path.dest + '/**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];

var remoteLocation = path.remote.dest;

function getFtpConnection(){
    return ftp.create({
        host: path.remote.host,
        port: 21,
        user: path.remote.user,
        password: path.remote.pass,
        parallel: 5,
        log: gutil.log
    })
}

gulp.task('remote-deploy-css',function(){
    var conn = getFtpConnection();
    return gulp.src(localCss, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

gulp.task('remote-deploy-js',function(){
    var conn = getFtpConnection();
    return gulp.src(localJs, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

gulp.task('remote-deploy-images',function(){
    var conn = getFtpConnection();
    return gulp.src(localImages, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

gulp.task('remote-deploy-files',function(){
    var conn = getFtpConnection();
    return gulp.src(localFiles, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

// DEFAULT task
gulp.task('default', gulp.series('css', 'js', 'watch'));